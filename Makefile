
# Where this file resides

root_dir := $(dir $(abspath $(lastword $(MAKEFILE_LIST))))

# When version changes, increment this number

VERSION := 1.0.0

# Determine where we are going to install things

prefix := $(HOME)/.local
bindir := $(prefix)/bin
datadir := $(prefix)/share/wodox

# According to the GNU Make documentation: "Every Makefile should define the
# variable INSTALL, which is the basic command for installing a file into the
# system.  Every Makefile should also define the variables INSTALL_PROGRAM and
# INSTALL_DATA."

INSTALL := install
INSTALL_PROGRAM := $(INSTALL)
INSTALL_DATA := $(INSTALL) -m 644

# These flags may be overriden by the user

CFLAGS ?= -g
CPPFLAGS ?= $(shell pkg-config --cflags sdl SDL_image SDL_mixer SDL_ttf SDL_gfx)
LDLIBS ?= $(shell pkg-config --libs sdl SDL_image SDL_mixer SDL_ttf SDL_gfx) -lm

# These flags are necessary

override CPPFLAGS += -DVERSION=\"$(VERSION)\"
override CPPFLAGS += -DPACKAGE=\"wodox\"
override CPPFLAGS += -DDATADIR=\"$(datadir)\"
override CPPFLAGS += -I$(root_dir)src

override CFLAGS += -std=gnu99
override CFLAGS += -fPIC
override CFLAGS += -MMD
override CFLAGS += -Wall
override CFLAGS += -Werror
override CFLAGS += -Wfatal-errors

# Compilation takes place in...

vpath %.c $(root_dir)src

# And now, the rules.

.PHONY: all
all: main

.PHONY: clean
clean:
	$(RM) *.o
	$(RM) *.d
	$(RM) main

.PHONY: install
install: all
	$(INSTALL) -d $(DESTDIR)$(bindir)
	$(INSTALL_PROGRAM) main $(DESTDIR)$(bindir)/wodox
	$(INSTALL) -d $(DESTDIR)$(datadir)/fonts
	$(INSTALL_DATA) data/fonts/acoustic_bass.ttf $(DESTDIR)$(datadir)/fonts
	$(INSTALL_DATA) data/fonts/j_d_handcrafted.ttf $(DESTDIR)$(datadir)/fonts
	$(INSTALL_DATA) data/fonts/underwood_champion.ttf $(DESTDIR)$(datadir)/fonts
	$(INSTALL) -d $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/title.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/effects.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/menu.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/circle.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/properties.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/manual.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/background.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/objects.png $(DESTDIR)$(datadir)/textures
	$(INSTALL_DATA) data/textures/frame.png $(DESTDIR)$(datadir)/textures
	$(INSTALL) -d $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/sideways $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/again $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/sidestep $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/the_maze $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/the_device $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/riding_the_wave $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/tipped $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/whirlwind $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/okay_now_panic $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/ascension $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/symmetry $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/all_together $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/round_we_go $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/glass_cage $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/push_the_button $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/just_ahead $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/modern_art $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/perspective $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/the_spiral_race $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/sokobany $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/make_a_way $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/filler $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/fall_down $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/doodle $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/descent $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/step_by_step $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/run $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/recovery $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/push_around $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/hit_n_run $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/bridge_building $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/3_18 $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/going_down $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/don_t_panic $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/cascade $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/reverse_stack $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/level.lst $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/timing_timing $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/warehouse $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/just_go $(DESTDIR)$(datadir)/levels
	$(INSTALL_DATA) data/levels/going_up $(DESTDIR)$(datadir)/levels
	$(INSTALL) -d $(DESTDIR)$(datadir)/effects
	$(INSTALL_DATA) data/effects/wodox.wav $(DESTDIR)$(datadir)/effects
	$(INSTALL_DATA) data/effects/fail.wav $(DESTDIR)$(datadir)/effects
	$(INSTALL_DATA) data/effects/marker01.wav $(DESTDIR)$(datadir)/effects
	$(INSTALL_DATA) data/effects/open.wav $(DESTDIR)$(datadir)/effects
	$(INSTALL_DATA) data/effects/typewriter.wav $(DESTDIR)$(datadir)/effects
	$(INSTALL_DATA) data/effects/release.wav $(DESTDIR)$(datadir)/effects
	$(INSTALL_DATA) data/effects/press.wav $(DESTDIR)$(datadir)/effects
	$(INSTALL) -d $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/howtolink4.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/howtoplay.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/messages.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/Makefile.in $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/howtolink3.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/howtolink2.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/howtoedit.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/howtolink.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/credits.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/menus.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/actnames.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL_DATA) data/lang/it/levelnames.txt $(DESTDIR)$(datadir)/lang/it
	$(INSTALL) -d $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/howtolink4.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/howtoplay.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/messages.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/Makefile.in $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/howtolink3.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/howtolink2.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/howtoedit.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/howtolink.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/credits.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/menus.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/actnames.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL_DATA) data/lang/en/levelnames.txt $(DESTDIR)$(datadir)/lang/en
	$(INSTALL) -d $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/howtolink4.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/howtoplay.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/messages.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/Makefile.in $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/howtolink3.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/howtolink2.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/howtoedit.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/howtolink.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/credits.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/menus.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/actnames.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL_DATA) data/lang/fr/levelnames.txt $(DESTDIR)$(datadir)/lang/fr
	$(INSTALL) -d $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/howtolink4.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/howtoplay.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/messages.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/Makefile.in $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/howtolink3.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/howtolink2.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/howtoedit.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/howtolink.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/credits.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/menus.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/actnames.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL_DATA) data/lang/es/levelnames.txt $(DESTDIR)$(datadir)/lang/es
	$(INSTALL) -d $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) data/music/track2.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) data/music/track3.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) data/music/track0.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) data/music/track4.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) data/music/track6.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) data/music/track5.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) data/music/track1.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL_DATA) data/music/track7.ogg $(DESTDIR)$(datadir)/music
	$(INSTALL) -d $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Simple $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Impossible $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Many_boxes_1 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Different_Angle $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Automation_1 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Many_boxes_4 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Uninteresting $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Conveyor_belts $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Many_boxes_3 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/High_up $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Buu!_Monster $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/The_Stack_Trick $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Electron-holes $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Many_boxes_2 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Automation_2 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Reverse_stack $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Testing_limits $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Automation_3 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Walking_on_boxes $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Machine_test_2 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Automation_4 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Sokoban_1 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Doodle $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Boss_Alert! $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Many_boxes_5 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Many_boxes_6 $(DESTDIR)$(datadir)/sandbox
	$(INSTALL_DATA) data/sandbox/Machine_test_1 $(DESTDIR)$(datadir)/sandbox

objects += wodox.o
objects += core.o
objects += lang.o
objects += user.o
objects += edit_circuit.o
objects += edit.o
objects += edit_file.o
objects += edit_parse.o
objects += edit_render.o
objects += edit_rotate.o
objects += edit_shift.o
objects += media.o
objects += menu.o
objects += play_circuit.o
objects += play_load.o
objects += play_object.o
objects += play.o
objects += play_render.o
objects += play_state.o
objects += play_update.o

main: $(objects)
	$(CC) $(CFLAGS) $(LDFLAGS) -o $@ $^ $(LOADLIBES) $(LDLIBS)

-include $(shell find -name "*.d")
