# syntax=docker/dockerfile:1

FROM debian

WORKDIR /workspace

RUN apt update && apt --yes install \
            build-essential \
            libsdl1.2-dev \
            libsdl-image1.2-dev \
            libsdl-mixer1.2-dev \
            libsdl-ttf2.0-dev \
            libsdl-gfx1.2-dev 

